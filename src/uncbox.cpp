/*
 * uncbox.cpp
 *
 *  Created on: 18.01.2017
 *      Author: gocht
 */

#include <uncbox.h>
#include <uncore_error.h>

namespace uncore
{

/** Initialize a box.
 *
 * This function gets the status, control and filter counter for each field by using
 * x86_adapt_lookup_ci_name().
 * Moreover it checks for all available counter slots in a uncore box.
 *
 * @param _box_name x86_a name of a certain box (like "U" or "S0")
 * @param _fixed are there none, one (single), or more (multiple) fixed counters
 * @param _norm are there none, one (single), or more (multiple) norm counters
 *
 */
unc_box::unc_box(std::string _box_name, std::string _box_prefix, pair_type _fixed, pair_type _norm)
    : box_name(_box_name), box_prefix(_box_prefix), fixed_typs(_fixed), norm_typs(_norm)
{
    box_status_name = box_name + "_PMON_STATUS";

    if (!(x86_adapt_lookup_ci_name(X86_ADAPT_DIE, box_status_name.c_str()) > 0))
    {
        throw box_error(box_name, "Box does not exist");
    }

    status = x86_adapt_lookup_ci_name(X86_ADAPT_DIE, box_status_name.c_str());
    ctl = x86_adapt_lookup_ci_name(X86_ADAPT_DIE, std::string(box_name + "_PMON_BOX_CTL").c_str());

    filter0 =
        x86_adapt_lookup_ci_name(X86_ADAPT_DIE, std::string(box_name + "_PMON_FILTER0").c_str());
    filter1 =
        x86_adapt_lookup_ci_name(X86_ADAPT_DIE, std::string(box_name + "_PMON_FILTER1").c_str());

    std::string box_fixed_ctl;
    std::string box_fixed_ctr;

    if (fixed_typs == single)
    {
        box_fixed_ctl = box_name + "_PMON_FIXED_CTL";
        box_fixed_ctr = box_name + "_PMON_FIXED_CTR";
    }
    else if (fixed_typs == multi)
    {
        box_fixed_ctl = box_name + "_PMON_FIXED_CTL" + std::to_string(fixed.size());
        box_fixed_ctr = box_name + "_PMON_FIXED_CTR" + std::to_string(fixed.size());
    }

    while ((fixed_typs != none) &&
           (x86_adapt_lookup_ci_name(X86_ADAPT_DIE, box_fixed_ctl.c_str()) > 0))
    {
        unc_pair pair;
        pair.ctl = x86_adapt_lookup_ci_name(X86_ADAPT_DIE, box_fixed_ctl.c_str());
        pair.ctr = x86_adapt_lookup_ci_name(X86_ADAPT_DIE, box_fixed_ctr.c_str());
        pair.used = false;
        fixed.push_back(pair);

        if (fixed_typs == multi)
        {
            box_fixed_ctl = box_name + "_PMON_FIXED_CTL" + std::to_string(fixed.size());
            box_fixed_ctr = box_name + "_PMON_FIXED_CTR" + std::to_string(fixed.size());
        }
        else
        {
            break;
        }
    }

    std::string box_norm_ctl;
    std::string box_norm_ctr;

    if (norm_typs == single)
    {
        box_norm_ctl = box_name + "_PMON_CTL";
        box_norm_ctr = box_name + "_PMON_CTR";
    }
    else if (norm_typs == multi)
    {
        box_norm_ctl = box_name + "_PMON_CTL" + std::to_string(norm.size());
        box_norm_ctr = box_name + "_PMON_CTR" + std::to_string(norm.size());
    }

    while (
        (norm_typs != none) && (x86_adapt_lookup_ci_name(X86_ADAPT_DIE, box_norm_ctl.c_str()) > 0))
    {
        unc_pair pair;
        pair.ctl = x86_adapt_lookup_ci_name(X86_ADAPT_DIE, box_norm_ctl.c_str());
        pair.ctr = x86_adapt_lookup_ci_name(X86_ADAPT_DIE, box_norm_ctr.c_str());
        pair.used = false;
        norm.push_back(pair);

        if (norm_typs == multi)
        {
            box_norm_ctl = box_name + "_PMON_CTL" + std::to_string(norm.size());
            box_norm_ctr = box_name + "_PMON_CTR" + std::to_string(norm.size());
        }
        else
        {
            break;
        }
    }
}

unc_box::~unc_box()
{
}

/** Reset a box.
 *
 * Resets a certain box by calling x86_adapt_set_setting() .
 *
 * Throws an box_error() in case of an error.
 *
 * @param x86a_fd file descriptor for x86_adapt_set_setting().
 *
 */
void unc_box::reset_box(int x86a_fd)
{
    if (!(box_name == "U"))
    {
        int ret = x86_adapt_set_setting(x86a_fd, ctl, 0x3);
        if (ret != 8)
        {
            throw box_error(box_name, "Failed to reset box");
        }
    }
    for (auto &ctr : norm)
    {
        ctr.used = false;
        ctr.cpu = -1;
        ctr.event_name = "";
    }

    for (auto &ctr : fixed)
    {
        ctr.used = false;
        ctr.cpu = -1;
        ctr.event_name = "";
    }
}

/** Setup of normal counters
 *
 * This function sets a normal counter to the event encoded in enc. If an error occurs box_error()
 * is thrown.
 * The function uses x86_adapt_get_setting() and x86_adapt_set_setting().
 *
 * @param x86a_fd x86_adapt file handle for x86_adapt_get_setting() and x86_adapt_set_setting().
 * @param enc encoding of the counter to measure. Returned by pfm_get_os_event_encoding()
 * @param cpu_id sets the associated CPU. Might be needed if multithread reading is implemented.
 * @param event_name name of the event to measure, for lated identification.
 *
 */
void unc_box::set_norm_counter(
    int x86a_fd, pfm_pmu_encode_arg_t &enc, int cpu_id, std::string event_name)
{
    uint ctr = 0;
    int ret = 0;
    while ((ctr < norm.size()) && norm[ctr].used)
    {
        ctr++;
    }
    if (ctr >= norm.size())
    {
        throw free_counter_error(
            box_name, "No free counter available for event, can't load: " + event_name);
    }

    uint64_t data;
    switch (enc.count)
    {
    case 3:
        data = 0;
        ret = x86_adapt_get_setting(x86a_fd, filter1, &data);
        if (!ret)
        {
            throw box_error(box_name, "Failed to read filter register 1 for event");
        }
        data |= enc.codes[2];
        ret = x86_adapt_set_setting(x86a_fd, filter1, data);
        if (ret != 8)
        {
            throw box_error(box_name, "Failed to write filter register 1 for event");
        }
    case 2:
        data = 0;
        ret = x86_adapt_get_setting(x86a_fd, filter0, &data);
        if (!ret)
        {
            throw box_error(box_name, "Failed to read filter register 0 for event");
        }
        data |= enc.codes[1];
        ret = x86_adapt_set_setting(x86a_fd, filter0, data);
        if (ret != 8)
        {
            throw box_error(box_name, "Failed to write filter register 0 for event");
        }
    case 1:
        ret = x86_adapt_set_setting(x86a_fd, norm[ctr].ctl, enc.codes[0] | (1u << 22u));
        if (ret != 8)
        {
            throw box_error(box_name, "Failed to write counter config for event");
        }
        break;
    default:
        throw box_error(box_name, "Unknown event encode size");
    }
    // if every thing worked and we survived, we can now say that the counter is used :-)
    norm[ctr].used = true;
    norm[ctr].cpu = cpu_id;
    norm[ctr].event_name = event_name;
}

/** Setup of normal counters
 *
 * This function sets a fixed counter to the event encoded in enc. If an error occurs box_error() is
 * thrown.
 * The function uses x86_adapt_set_setting().
 *
 * @param x86a_fd x86_adapt file handle for x86_adapt_set_setting().
 * @param enc encoding of the counter to measure. Returned by pfm_get_os_event_encoding()
 * @param cpu_id sets the associated CPU. Might be needed if multithread reading is implemented.
 * @param event_name name of the event to measure, for lated identification.
 *
 */
void unc_box::set_fixed_counter(
    int x86a_fd, pfm_pmu_encode_arg_t &enc, int cpu_id, std::string event_name)
{
    auto ret = x86_adapt_set_setting(x86a_fd, fixed[0].ctl, enc.codes[0] | (1u << 22u));
    if (ret != 8)
    {
        throw box_error(box_name, "Failed to write counter config for event ");
    }
    fixed[0].used = true;
    fixed[0].cpu = cpu_id;
    fixed[0].event_name = event_name;
}

/** Reads the data for all pairs.
 *
 * @param x86a_fd x86_adapt file handle for x86_adapt_get_setting().
 * @return a map containing the name of the event, and the value of the counter.
 *
 */
std::map<std::string, uint64_t> unc_box::read_counter(int x86a_fd)
{
    std::map<std::string, uint64_t> result;
    int ret = 0;
    for (auto &fix : fixed)
    {
        if (fix.used)
        {
            uint64_t data = 0;
            ret = x86_adapt_get_setting(x86a_fd, fix.ctr, &data);
            if (!ret)
            {
                std::cerr << "Error while reading event" << fix.event_name;
            }
            else
            {
                result[fix.event_name] = data;
            }
        }
        else
        {
            break;
        }
    }
    for (auto &nor : norm)
    {
        if (nor.used)
        {
            uint64_t data = 0;
            ret = x86_adapt_get_setting(x86a_fd, nor.ctr, &data);
            if (!ret)
            {
                std::cerr << "Error while reading event" << nor.event_name;
            }
            else
            {
                result[nor.event_name] = data;
            }
        }
        else
        {
            break;
        }
    }

    return result;
}

const std::string unc_box::get_prefix()
{
    return box_prefix;
}

/** equal operator.
 *
 */
unc_box &unc_box::operator=(unc_box other)
{
    box_name = other.box_name;
    box_prefix = other.box_prefix;
    fixed_typs = other.fixed_typs;
    norm_typs = other.norm_typs;
    status = other.status;
    ctl = other.ctl;
    filter0 = other.filter0;
    filter1 = other.filter1;
    fixed = other.fixed;
    norm = other.norm;
    cpu = other.cpu;
    box_status_name = other.box_status_name;
    return *this;
}
} /* namespace reade_uncore */
