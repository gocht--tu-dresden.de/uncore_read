/*
 * main.cpp
 *
 *  Created on: 11.01.2017
 *      Author: gocht
 */

#include <iostream>
#include <sstream>
#include <string>

#include <readuncore.h>

int main(int argc, char *argv[])
{
    uncore::read_uncore uncore;
    std::cout << "Hello world" << std::endl;
    std::istringstream avail_counter("hswep_unc_cbo0::UNC_C_CLOCKTICKS,"
                                     "hswep_unc_cbo0::UNC_C_COUNTER0_OCCUPANCY,"
                                     "hswep_unc_cbo0::UNC_C_LLC_LOOKUP:DATA_READ,"
                                     "hswep_unc_cbo0::UNC_C_LLC_LOOKUP:WRITE,"
                                     "hswep_unc_cbo1::UNC_C_CLOCKTICKS,"
                                     "hswep_unc_cbo1::UNC_C_COUNTER0_OCCUPANCY,"
                                     "hswep_unc_cbo1::UNC_C_LLC_LOOKUP:DATA_READ,"
                                     "hswep_unc_cbo1::UNC_C_LLC_LOOKUP:WRITE,"
                                     "hswep_unc_cbo10::UNC_C_CLOCKTICKS,"
                                     "hswep_unc_cbo10::UNC_C_COUNTER0_OCCUPANCY,"
                                     "hswep_unc_cbo10::UNC_C_LLC_LOOKUP:DATA_READ,"
                                     "hswep_unc_cbo10::UNC_C_LLC_LOOKUP:WRITE,"
                                     "hswep_unc_cbo11::UNC_C_CLOCKTICKS,"
                                     "hswep_unc_cbo11::UNC_C_COUNTER0_OCCUPANCY,"
                                     "hswep_unc_cbo11::UNC_C_LLC_LOOKUP:DATA_READ,"
                                     "hswep_unc_cbo11::UNC_C_LLC_LOOKUP:WRITE,"
                                     "hswep_unc_cbo2::UNC_C_CLOCKTICKS,"
                                     "hswep_unc_cbo2::UNC_C_COUNTER0_OCCUPANCY,"
                                     "hswep_unc_cbo2::UNC_C_LLC_LOOKUP:DATA_READ,"
                                     "hswep_unc_cbo2::UNC_C_LLC_LOOKUP:WRITE,"
                                     "hswep_unc_cbo3::UNC_C_CLOCKTICKS,"
                                     "hswep_unc_cbo3::UNC_C_COUNTER0_OCCUPANCY,"
                                     "hswep_unc_cbo3::UNC_C_LLC_LOOKUP:DATA_READ,"
                                     "hswep_unc_cbo3::UNC_C_LLC_LOOKUP:WRITE,"
                                     "hswep_unc_cbo4::UNC_C_CLOCKTICKS,"
                                     "hswep_unc_cbo4::UNC_C_COUNTER0_OCCUPANCY,"
                                     "hswep_unc_cbo4::UNC_C_LLC_LOOKUP:DATA_READ,"
                                     "hswep_unc_cbo4::UNC_C_LLC_LOOKUP:WRITE,"
                                     "hswep_unc_cbo5::UNC_C_CLOCKTICKS,"
                                     "hswep_unc_cbo5::UNC_C_COUNTER0_OCCUPANCY,"
                                     "hswep_unc_cbo5::UNC_C_LLC_LOOKUP:DATA_READ,"
                                     "hswep_unc_cbo5::UNC_C_LLC_LOOKUP:WRITE,"
                                     "hswep_unc_cbo6::UNC_C_CLOCKTICKS,"
                                     "hswep_unc_cbo6::UNC_C_COUNTER0_OCCUPANCY,"
                                     "hswep_unc_cbo6::UNC_C_LLC_LOOKUP:DATA_READ,"
                                     "hswep_unc_cbo6::UNC_C_LLC_LOOKUP:WRITE,"
                                     "hswep_unc_cbo7::UNC_C_CLOCKTICKS,"
                                     "hswep_unc_cbo7::UNC_C_COUNTER0_OCCUPANCY,"
                                     "hswep_unc_cbo7::UNC_C_LLC_LOOKUP:DATA_READ,"
                                     "hswep_unc_cbo7::UNC_C_LLC_LOOKUP:WRITE,"
                                     "hswep_unc_cbo8::UNC_C_CLOCKTICKS,"
                                     "hswep_unc_cbo8::UNC_C_COUNTER0_OCCUPANCY,"
                                     "hswep_unc_cbo8::UNC_C_LLC_LOOKUP:DATA_READ,"
                                     "hswep_unc_cbo8::UNC_C_LLC_LOOKUP:WRITE,"
                                     "hswep_unc_cbo9::UNC_C_CLOCKTICKS,"
                                     "hswep_unc_cbo9::UNC_C_COUNTER0_OCCUPANCY,"
                                     "hswep_unc_cbo9::UNC_C_LLC_LOOKUP:DATA_READ,"
                                     "hswep_unc_cbo9::UNC_C_LLC_LOOKUP:WRITE,"
                                     "hswep_unc_ha0::UNC_H_CLOCKTICKS,"
                                     "hswep_unc_ha0::UNC_H_CONFLICT_CYCLES,"
                                     "hswep_unc_ha0::UNC_H_DIRECT2CORE_COUNT,"
                                     "hswep_unc_ha0::UNC_H_DIRECT2CORE_CYCLES_DISABLED,"
                                     "hswep_unc_ha1::UNC_H_CLOCKTICKS,"
                                     "hswep_unc_ha1::UNC_H_CONFLICT_CYCLES,"
                                     "hswep_unc_ha1::UNC_H_DIRECT2CORE_COUNT,"
                                     "hswep_unc_ha1::UNC_H_DIRECT2CORE_CYCLES_DISABLED,"
                                     "hswep_unc_pcu::UNC_P_CLOCKTICKS,"
                                     "hswep_unc_pcu::UNC_P_CORE0_TRANSITION_CYCLES,"
                                     "hswep_unc_pcu::UNC_P_CORE1_TRANSITION_CYCLES,"
                                     "hswep_unc_pcu::UNC_P_CORE2_TRANSITION_CYCLES,"
                                     "hswep_unc_ubo::UNC_U_EVENT_MSG:DOORBELL_RCVD,"
                                     "hswep_unc_ubo::UNC_U_PHOLD_CYCLES:ASSERT_TO_ACK");

    std::vector<std::string> lines;
    for (std::string line; std::getline(avail_counter, line, ',');)
    {
        lines.push_back(line);
    }
    uncore.set_counters(lines);

    for (int i = 0; i < 10; i++)
    {
        auto result = uncore.read_counter();
        for (auto &res : result)
        {
            std::cout << "node0:" << std::endl;
            for (auto &r : res)
            {
                std::cout << "\t" << r.first << "\t" << r.second << std::endl;
            }
        }
        sleep(10);
    }
}
