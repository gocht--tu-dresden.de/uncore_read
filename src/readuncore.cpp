/*
 * readuncore.cpp
 *
 *  Created on: 11.01.2017
 *      Author: gocht
 */

#include "readuncore.h"

#include <dirent.h>
#include <iostream>

#include <assert.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>

#include <uncore_error.h>

namespace uncore
{
/** initializes the needed data structures
 *
 * This function gets the number of nodes available. Next it registers the different boxes for each
 * node and resets
 * them.
 *
 */
read_uncore::read_uncore()
{
    int ret;

    ret = x86_adapt_init();

    if (ret != 0)
    {
        std::cerr << "Could not initialize x86_adapt library, return code: " << ret << std::endl;
    }

    ret = pfm_initialize();
    if (ret != PFM_SUCCESS)
    {
        std::cerr << "cannot initialize pfm library : " << pfm_strerror(ret) << std::endl;
    }

    node_num = x86_adapt_get_nr_avaible_devices(X86_ADAPT_DIE);

    assert(node_num > 0);

    cpus = sysconf(_SC_NPROCESSORS_CONF);
    ht_enabled = is_ht_enabled();

    for (int i = 0; i < node_num; i++)
    {

        int cpus_per_node;
        if (ht_enabled)
        {
            cpus_per_node = cpus / 2 / node_num;
        }
        else
        {
            cpus_per_node = cpus / node_num;
        }
        node tmp_node(i, cpus_per_node);

        nodes.push_back(tmp_node);
    }
}

read_uncore::~read_uncore()
{
    nodes.clear();
    x86_adapt_finalize();
}

/** sets the counter with the name event_name
 *
 * This function gets the fully qualified event name from libpfm, translates the box to the
 * x86_adapt box name, and
 * sets the box up, using the event and filter informations from libpfm.
 *
 * This function might throw an read_uncore_error, if a certain event is not found. If the event is
 * thrown, the state
 * of the counter is not specified.
 *
 * @param events map with event's to set per box, key is the box name, value is the full name of the
 * event (including box name)
 */
void read_uncore::set_counters(box_set &events)
{
    for (auto &node : nodes)
    {
        node.reset_boxes();
    }

    for (auto &box : events)
    {
        std::string event_box = box.first;
        for (auto &event : box.second)
        {
            std::string event_name = "";
            if (nodes.begin()->is_multibox(event_box))
            {
                event_name = event_box + "0::" + event;
            }
            else
            {
                event_name = event_box + "::" + event;
            }

            int ret = 0;
            pfm_pmu_encode_arg_t enc = {0};
            char *fstr = NULL;
            enc.fstr = &fstr;

            ret = pfm_get_os_event_encoding(
                event_name.c_str(), PFM_PLM0 | PFM_PLM3, PFM_OS_NONE, &enc);
            if (ret != PFM_SUCCESS)
            {
                std::cerr << "Failed to encode event: " << event_name << " [" << pfm_strerror(ret)
                          << "]" << std::endl;
                continue;
            }

            std::string quallified_pfm_event_name(fstr);

            for (auto &node : nodes)
            {
                int scatter_id = node.get_scatter_id(event_name);

                /* corner case for fixed counters */
                /* only happens with imc boxes */
                bool fixed = false;

                if ((quallified_pfm_event_name.find("unc_imc") != std::string::npos) &&
                    (quallified_pfm_event_name.find("UNC_M_CLOCKTICKS") != std::string::npos))
                {
                    fixed = true;
                }

                int node_cpu = 0;
                int cpu_id = 0;
                for (int i = 0; i < cpus; i++)
                {
                    if (node_of_cpu(i) == node.get_node_id())
                    {
                        if (node_cpu == scatter_id)
                        {
                            cpu_id = i;
                        }
                        else
                        {
                            node_cpu++;
                        }
                    }
                }

                try
                {
                    node.set_up_box(event_box, fixed, enc, cpu_id, event);
                }
                catch (read_uncore_error &e)
                {
                    e.append_event_name(
                        event_box + std::string("::") + event, quallified_pfm_event_name);
                    throw;
                }
            }
        }
    }

    for (auto &node : nodes)
    {
        node.unfreeze();
    }
}

/** returns the reading of all previously set uncore counters
 *
 * @return all collected values related to a setup name.
 *
 */
box_values read_uncore::read_counter()
{
    box_values result;
    for (auto &node : nodes)
    {
        auto tmp_results = node.read_counters();
        for (auto &tmp_result : tmp_results)
        {
            auto &name = tmp_result.first;
            auto &values = tmp_result.second;
            result[name].insert(result[name].end(), values.begin(), values.end());
        }
    }
    return result;
}

/* hyperthreading is disabled if if the thread_siblings_list is only 2 bytes, 0 and EOF */
int is_ht_enabled(void)
{
    int fd = open("/sys/devices/system/cpu/cpu0/topology/thread_siblings_list", O_RDONLY);
    char buf[64] = {0};
    if (fd < 0)
    {
        return -1;
    }
    ssize_t count = read(fd, buf, 64);
    switch (count)
    {
    case 0:
        return -1;
    case 2:
        return 0;
    default:
        return 1;
    }
}

/**
 * Returns the number of nodes in this system.
 * NOTE: On AMD_fam15h you should use get_nr_packages() from x86_energy_source.
 */
int get_nr_packages(void)
{
    int n, nr_packages = 0;
    struct dirent **namelist;
    std::string path("/sys/devices/system/node");

    n = scandir(path.c_str(), &namelist, NULL, alphasort);
    while (n--)
    {
        if (!strncmp(namelist[n]->d_name, "node", 4))
        {
            nr_packages++;
        }
        free(namelist[n]);
    }
    free(namelist);

    return nr_packages;
}

/**
 * Returns the correponding node of the give cpu.
 * If the cpu can not be found on this system -1 is returned.
 */
int node_of_cpu(int __cpu)
{
    int nr_packages = get_nr_packages();
    char path[64];

    for (int node = 0; node < nr_packages; node++)
    {
        int sz = snprintf(path, sizeof(path), "/sys/devices/system/node/node%d/cpu%d", node, __cpu);
        assert(sz < static_cast<int>(sizeof(path)));
        struct stat stat_buf;
        int stat_ret = stat(path, &stat_buf);
        if (0 == stat_ret)
        {
            return node;
        }
    }
    return -1;
}
}
