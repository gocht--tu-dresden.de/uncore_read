/*
 * node.cpp
 *
 *  Created on: 18.01.2017
 *      Author: gocht
 */

#include <algorithm>
#include <node.h>
#include <uncore_error.h>

namespace uncore
{

/** inits the node.
 *
 * This function initialize each node for itself. Moreover it initializes the boxes.
 *
 * Finally it resets all boxes by calling reset_boxes().
 *
 * @param _node_id id of the current node
 * @param _cpus amunt of CPU's per node.
 *
 */
node::node(int _node_id, int _cpus) : node_id(_node_id), cpus(_cpus), scatter_id(0)
{
    x86a_fd = x86_adapt_get_device(X86_ADAPT_DIE, node_id);
    if (x86a_fd < 0)
    {
        throw read_uncore_error(
            "Could not get fd on node " + std::to_string(node_id) + "for resetting the boxes");
    }

    global_ctl = x86_adapt_lookup_ci_name(X86_ADAPT_DIE, "GLOBAL_PMON_BOX_CTL");
    global_status = x86_adapt_lookup_ci_name(X86_ADAPT_DIE, "GLOBAL_PMON_STATUS");
    global_config = x86_adapt_lookup_ci_name(X86_ADAPT_DIE, "GLOBAL_PMON_CONFIG");

    if (global_ctl < 0)
    {
        throw read_uncore_error("global_ctl not found on node " + std::to_string(node_id));
    }
    if (global_status < 0)
    {
        throw read_uncore_error("global_status  not found on node " + std::to_string(node_id));
    }
    if (global_config < 0)
    {
        throw read_uncore_error("global_config not found on node " + std::to_string(node_id));
    }

    boxes["hswep_unc_ubo"] = unc_box("U", "hswep_unc_ubo", single, multi);

    boxes["hswep_unc_pcu"] = unc_box("PCU", "hswep_unc_pcu", none, multi);

    append_multi_box("S", "hswep_unc_sbo", none, multi);

    append_multi_box("C", "hswep_unc_cbo", none, multi);

    append_multi_box("HA", "hswep_unc_ha", none, multi);

    append_multi_box("IMC0_CHAN", "hswep_unc_imc", single, multi);

    append_multi_box("IMC1_CHAN", "hswep_unc_imc", single, multi);

    append_multi_box(
        "IRP", "hswep_unc_irp", none, multi); // not sure if the papi counter is correct

    append_multi_box("QPI", "hswep_unc_qpi", none, multi);

    boxes["hswep_unc_r2pcie"] = unc_box("R2PCIe", "hswep_unc_r2pcie", none, multi);

    append_multi_box("R3QPI0_Link_", "hswep_unc_r3qpi", none, multi);

    // reset boxes
    reset_boxes();
}

/** Finalizes the node.
 *
 * Finalizes all boxes, and than call x86_adapt_put_device().
 *
 */
node::~node()
{
    boxes.clear();
    x86_adapt_put_device(X86_ADAPT_DIE, x86a_fd);
}

/** This function adds multiple boxes with numbers in the name to the list, for example "S[0-12]"
 *
 * @param box_prefix prefix of box (S for S-box)
 * @param pfm_prefix pfm prefix of box
 * @param fixed are there none, one (single), or more (multiple) fixed counters
 * @param norm are there none, one (single), or more (multiple) norm counters
 */
void node::append_multi_box(
    std::string box_prefix, std::string pfm_prefix, pair_type fixed, pair_type norm)
{

    int pfm_index = 0;
    /*
     * imcbox corner case handling
     * papi enumerates the channels from both controllers form 0-7
     * but adapt enumerates the channels for each controller (0-3 for each IMC, and 0-1 for the IMC)
     */

    if (box_prefix.find("IMC1") != std::string::npos)
    {
        pfm_index = 4;
    }

    int box_index = 0;
    auto box_name = box_prefix + std::to_string(box_index);
    auto pfm_name = pfm_prefix + std::to_string(pfm_index);

    while (
        x86_adapt_lookup_ci_name(X86_ADAPT_DIE, std::string(box_name + "_PMON_STATUS").c_str()) > 0)
    {
        boxes[pfm_name] = unc_box(box_name, pfm_prefix, fixed, norm);

        box_index++;
        pfm_index++;

        box_name = box_prefix + std::to_string(box_index);
        pfm_name = pfm_prefix + std::to_string(pfm_index);
    }
    available_multi_boxes[pfm_prefix] = box_index;
}

/** this function sets the given enc for the box with the name box name (including number for
 * multiboxes)
 *
 * @param box_name name of the box
 * @param enc pfm_pmu_encode_arg_t returned from pfm_get_os_event_encoding
 * @param cpu_id cpu which is supposed to read the counter
 * @param event_name name under which the event is returned
 */
void node::set_up_individual_box(const std::string &box_name,
    bool fixed,
    pfm_pmu_encode_arg_t &enc,
    int cpu_id,
    const std::string &event_name)
{
    if (fixed)
    {
        boxes[box_name].set_fixed_counter(x86a_fd, enc, cpu_id, event_name);
        if (std::find(setup_boxes.begin(), setup_boxes.end(), box_name) == setup_boxes.end())
        {
            setup_boxes.push_back(box_name);
        }
    }
    else
    {
        boxes[box_name].set_norm_counter(x86a_fd, enc, cpu_id, event_name);
        if (std::find(setup_boxes.begin(), setup_boxes.end(), box_name) == setup_boxes.end())
        {
            setup_boxes.push_back(box_name);
        }
    }
}

/** this function sets the given enc for all boxes with the prefix box_prefix
 *
 * @param box_prefix prefix of the box. For multiboxes, box name without number
 * @param enc pfm_pmu_encode_arg_t returned from pfm_get_os_event_encoding
 * @param cpu_id cpu which is supposed to read the counter (I think this has currently no meaning)
 * @param event_name name under which the event is returned
 */
void node::set_up_box(std::string box_prefix,
    bool fixed,
    pfm_pmu_encode_arg_t &enc,
    int cpu_id,
    std::string event_name)
{
    if (is_multibox(box_prefix))
    {
        for (int box_index = 0; box_index < available_multi_boxes[box_prefix]; box_index++)
        {
            std::string box_name = box_prefix + std::to_string(box_index);
            set_up_individual_box(box_name, fixed, enc, cpu_id, event_name);
        }
    }
    else
    {
        set_up_individual_box(box_prefix, fixed, enc, cpu_id, event_name);
    }
}

/** returns the scattered id for the given node.
 *
 * This function is user to distribute the measurement overhead between the different cores. It
 * is
 * ensured, that the
 * cboxes are associated with their individual core.
 *
 * @param event_name name of the event to scatter
 * @param _node node on which the element shall be scattered
 *
 * @return id of the core where the measurement should be taken
 *
 */
int node::get_scatter_id(std::string event_name)
{
    /* wrap around scatter id */
    scatter_id = scatter_id % cpus;

    auto pos_unc_cbo = event_name.find("unc_cbo");
    /* assign cbox event to correponding core */
    if (pos_unc_cbo != std::string::npos)
    {
        int len_unc = std::string("unc_cbo").length();
        len_unc = len_unc + pos_unc_cbo;
        int len_name = event_name.length();
        int num = stoi(event_name.substr(len_unc, len_name));
        return num;
    }
    return scatter_id++;
}

/** unfreezes all counters. Needs to be calles afer reset.
 *
 */
void node::unfreeze()
{
    auto ret = x86_adapt_set_setting(x86a_fd, global_ctl, (1u << 29u));
    if (ret != 8)
    {
        throw read_uncore_error("Failed to unfreeze counter for node " + std::to_string(node_id));
    }
}

/** freezes all counters. Is usually called by reset.
 *
 */
void node::freeze()
{
    auto ret = x86_adapt_set_setting(x86a_fd, global_ctl, (1u << 31u));
    if (ret != 8)
    {
        throw read_uncore_error("Failed to freeze counter for node " + std::to_string(node_id));
    }
}

/** resets all boxes.
 *
 * This function is used when all boxes need to be reseted. This might be the case if a new set
 * of
 * counters shall be
 * measured. After the reset, unfreeze() needs to be called to start counting.
 */
void node::reset_boxes()
{
    freeze();
    for (auto &box : boxes)
    {
        box.second.reset_box(x86a_fd);
    }
    setup_boxes.clear();
}

/** Reads the data for configured counters.
 *
 * @return a map of maps. The first map contains the name of the box,
 *  the second the name of the event, and the value of the counter.
 *
 */
box_values node::read_counters()
{
    box_values result;
    for (auto &box : setup_boxes)
    {
        box_result data = boxes[box].read_counter(x86a_fd);
        auto box_prefix = boxes[box].get_prefix();
        if (data.size() > 0)
        {
            result[box_prefix].push_back(data);
        }
    }
    return result;
}

/** returns the node_id;
 *
 * @return node_id
 */
int node::get_node_id()
{
    return node_id;
}

/** returns true if the given box is a multibox
 *
 */
bool node::is_multibox(std::string box_prefix)
{
    return available_multi_boxes.find(box_prefix) != available_multi_boxes.end();
}

} /* namespace reade_uncore */
