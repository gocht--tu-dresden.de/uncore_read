cmake_minimum_required(VERSION 3.11)

add_subdirectory(extern)

add_library(read-uncore-lib
    STATIC
    ${CMAKE_CURRENT_LIST_DIR}/src/readuncore.cpp
    ${CMAKE_CURRENT_LIST_DIR}/src/node.cpp
    ${CMAKE_CURRENT_LIST_DIR}/src/uncbox.cpp)

target_include_directories(read-uncore-lib
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/include
)
    
target_link_libraries(read-uncore-lib
    PUBLIC
	   libpfm::libpfm x86_adapt::x86_adapt
)
set_target_properties(read-uncore-lib PROPERTIES POSITION_INDEPENDENT_CODE ON)
target_compile_features(read-uncore-lib PUBLIC cxx_std_14)
target_compile_options(read-uncore-lib PRIVATE $<$<CONFIG:Debug>:-g -Wall>)
