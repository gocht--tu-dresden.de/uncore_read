/*
 * node.h
 *
 *  Created on: 18.01.2017
 *      Author: gocht
 */

#ifndef NODE_H_
#define NODE_H_

#include <iostream>
#include <map>
#include <vector>
#include <x86_adapt.h>

extern "C" {
#include <perfmon/perf_event.h>
#include <perfmon/pfmlib.h>
#include <perfmon/pfmlib_perf_event.h>
}

#include <error.h>
#include <uncbox.h>

namespace uncore
{

using box_set = std::map<std::string, std::vector<std::string>>;

using box_result = std::map<std::string, uint64_t>;
using box_values = std::map<std::string, std::vector<box_result>>;

class node
{
public:
    node(int _node_id, int _cpus);
    virtual ~node();
    void set_up_box(std::string box_name,
        bool fixed,
        pfm_pmu_encode_arg_t &enc,
        int cpu_id,
        std::string event_name);

    int get_scatter_id(std::string event_name);

    void freeze();
    void unfreeze();
    void reset_boxes();

    box_values read_counters();

    int get_node_id();
    bool is_multibox(std::string box_prefix);

private:
    int node_id;
    bool cpus;
    int scatter_id;
    int x86a_fd;
    std::map<std::string, unc_box> boxes;
    std::map<std::string, int> available_multi_boxes;
    std::vector<std::string> setup_boxes;
    int32_t global_ctl;
    int32_t global_status;
    int32_t global_config;

    void append_multi_box(
        std::string box_prefix, std::string pfm_prefix, pair_type fixed, pair_type norm);
    void set_up_individual_box(const std::string &box_name,
        bool fixed,
        pfm_pmu_encode_arg_t &enc,
        int cpu_id,
        const std::string &event_name);
};

} /* namespace reade_uncore */

#endif /* NODE_H_ */
