/*
 * readuncore.h
 *
 *  Created on: 11.01.2017
 *      Author: gocht
 */

#ifndef READUNCORE_H_
#define READUNCORE_H_

#include <map>
#include <string>
#include <vector>
#include <x86_adapt.h>

extern "C" {
#include <perfmon/perf_event.h>
#include <perfmon/pfmlib.h>
#include <perfmon/pfmlib_perf_event.h>
}

#include <node.h>

namespace uncore
{

class read_uncore
{
public:
    read_uncore();
    virtual ~read_uncore();
    void set_counters(box_set &events);
    box_values read_counter();

private:
    int node_num;
    int cpus;
    bool ht_enabled = false;

    std::vector<node> nodes;
};

int is_ht_enabled(void);
int get_nr_packages(void);
int node_of_cpu(int __cpu);
}
#endif /* READUNCORE_H_ */
