/*
 * uncbox.h
 *
 *  Created on: 18.01.2017
 *      Author: gocht
 */

#ifndef UNCBOX_H_
#define UNCBOX_H_

#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <x86_adapt.h>

extern "C" {
#include <perfmon/perf_event.h>
#include <perfmon/pfmlib.h>
#include <perfmon/pfmlib_perf_event.h>
}

namespace uncore
{

enum pair_type
{
    none,
    single,
    multi
};

class unc_box
{
public:
    unc_box() = default;
    unc_box(std::string _box_name, std::string _box_prefix, pair_type _fixed, pair_type _norm);
    virtual ~unc_box();

    void reset_box(int x86a_fd);
    void set_norm_counter(
        int x86a_fd, pfm_pmu_encode_arg_t &enc, int cpu_id, std::string event_name);
    void set_fixed_counter(
        int x86a_fd, pfm_pmu_encode_arg_t &enc, int cpu_id, std::string event_name);

    std::map<std::string, uint64_t> read_counter(int x86a_fd);

    const std::string get_prefix();

    unc_box &operator=(unc_box arg);

private:
    struct unc_pair
    {
        bool used;
        int cpu;
        std::string event_name;
        int32_t ctr;
        int32_t ctl;
    };

    std::string box_name;
    std::string box_prefix;
    pair_type fixed_typs = none;
    pair_type norm_typs = none;

    int32_t status = 0;
    int32_t ctl = 0;
    int32_t filter0 = 0;
    int32_t filter1 = 0;
    std::vector<unc_pair> fixed;
    std::vector<unc_pair> norm;

    int cpu = 0; /**used for distributing the measurement of the box between different CPU's*/
    std::string box_status_name;
};

} /* namespace reade_uncore */

#endif /* UNCBOX_H_ */
