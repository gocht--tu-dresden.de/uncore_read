/*
 * error.h
 *
 *  Created on: 18.01.2017
 *      Author: gocht
 */

#ifndef ERROR_H_
#define ERROR_H_

#include <exception>
#include <string>

namespace uncore
{
class read_uncore_error : public std::exception
{
public:
    read_uncore_error(const std::string &what_arg)
    {
        message = what_arg;
    }

    void append_event_name(std::string event_name, std::string event_name_pfm)
    {
        message += " \"";
        message += event_name;
        message += "\" or pfm name \"";
        message += event_name_pfm;
        message += "\"";
    }

    const char *what() const noexcept override
    {
        return message.c_str();
    }

private:
    std::string message;
};

class box_error : public std::exception
{
public:
    box_error(std::string box, std::string what_arg) : message(box + std::string(": ") + what_arg)
    {
    }

    const char *what() const noexcept override
    {
        return message.c_str();
    }

private:
    std::string message;
};

class free_counter_error : public box_error
{
public:
    free_counter_error(std::string box, std::string what_arg) : box_error(box, what_arg)
    {
    }
};

} /* namespace reade_uncore */

#endif /* ERROR_H_ */
